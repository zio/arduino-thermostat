/*
 * Inputs ADC Value from Thermistor and outputs Temperature in Celsius
 *  requires: include <math.h>
 * Utilizes the Steinhart-Hart Thermistor Equation:
 *    Temperature in Kelvin = 1 / {A + B[ln(R)] + C[ln(R)]3}
 *    where A = 0.001129148, B = 0.000234125 and C = 8.76741E-08
 *
 * These coefficients seem to work fairly universally, which is a bit of a 
 * surprise. 
 *
 * Schematic:
 *   [Ground] -- [10k-pad-resistor] -- | -- [thermistor] --[Vcc (5 or 3.3v)]
 *                                     |
 *                                Analog Pin 0
 *
 * In case it isn't obvious (as it wasn't to me until I thought about it), the analog ports
 * measure the voltage between 0v -> Vcc which for an Arduino is a nominal 5v, but for (say) 
 * a JeeNode, is a nominal 3.3v.
 *
 * The resistance calculation uses the ratio of the two resistors, so the voltage
 * specified above is really only required for the debugging that is commented out below
 *
 * Resistance = (1024 * PadResistance/ADC) - PadResistor 
 *
 * I have used this successfully with some CH Pipe Sensors (http://www.atcsemitec.co.uk/pdfdocs/ch.pdf)
 * which be obtained from http://www.rapidonline.co.uk.
 *
 */
 /*
  Melody
 
 Plays a melody 
 
 circuit:
 * 8-ohm speaker on digital pin 8
 
 created 21 Jan 2010
 modified 30 Aug 2011
 by Tom Igoe 

This example code is in the public domain.
 
 http://arduino.cc/en/Tutorial/Tone
 
 */
 
 
 /*
The circuit for DX LCD/keypad shield :
* LCD RS pin to digital pin 8
* LCD Enable pin to digital pin 9
* LCD D4 pin to digital pin 4
* LCD D5 pin to digital pin 5
* LCD D6 pin to digital pin 6
* LCD D7 pin to digital pin 7
* LCD R/W pin to ground
* 10K resistor:
* ends to +5V and ground
* wiper to LCD VO pin (pin 3)

*/

// include the library code:
#include <LiquidCrystal.h>

// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(8, 9, 4, 5, 6, 7);
int ButtonVoltage = 0;
int ButtonPressed = 0;
int Backlight = 10;
int fadeValue = 25;

#define BUTTON_SELECT    5
#define BUTTON_LEFT      4
#define BUTTON_DOWN      3
#define BUTTON_UP        2
#define BUTTON_RIGHT     1
#define BUTTON_NONE      0
 
 
/*************************************************
  * Public Constants
  *************************************************/

#include <math.h>
#include <pitches.h>

#define ThermistorPIN 1                 // Analog Pin 0
#define ATXGreenPIN 13 //  We have used A1 which maps to D15.

int switch_temp = 5;    // temperature of switching on hater
int delta = 2;          // allowed delta of switching on/off
int critical_temp = 2;  // temperature of buzzer ON

float hist_temp[] = {0.0, 10.0, 10.0, 0.0, 10.0, 0.0, 0.0, 0.0};    //array of previous temperature values for smoothing noise of T

int prev_state = 0;   // previous state of power: 0 - off, 1 - on

float vcc = 5.0;                       // only used for display purposes, if used
                                        // set to the measured Vcc.
float pad = 20000;                       // balance/pad resistor value, set this to
                                        // the measured resistance of your pad resistor
float thermr = 50000;                   // thermistor nominal resistance

int stat_on = 0;
int stat_off = 0; //just for on/off percentage

float Thermistor(int RawADC) {
  long Resistance;  
  float Temp;  // Dual-Purpose variable to save space.

  Resistance=((1024 * pad / RawADC) - pad); 
  Temp = log(Resistance); // Saving the Log(resistance) so not to calculate  it 4 times later
  Temp = 1 / (0.00096564 + (0.00021068 * Temp) + (0.000000085826 * Temp * Temp * Temp));
  Temp = Temp - 273.15;  // Convert Kelvin to Celsius                      

  // BEGIN- Remove these lines for the function not to display anything
  Serial.print("ADC: ");  lcd.setCursor (0,1); lcd.print ("ADC: ");
  Serial.print(RawADC);   lcd.setCursor (5,1); lcd.print (RawADC);
  Serial.print("/1024");                           // Print out RAW ADC Number
  //Serial.print(", vcc: ");
  //Serial.print(vcc,2);
  //Serial.print(", pad: ");
  //Serial.print(pad/1000,3);
  //Serial.print(" Kohms, Volts: "); 
  //Serial.print(((RawADC*vcc)/1024.0),3);   
  //Serial.print(", Resistance: "); 
  //Serial.print(Resistance);
  //Serial.print(" ohms, ");
  // END- Remove these lines for the function not to display anything

  // Uncomment this line for the function to return Fahrenheit instead.
  //temp = (Temp * 9.0)/ 5.0 + 32.0;                  // Convert to Fahrenheit
  return Temp;                                      // Return the Temperature
}


float smooth_temp(float curr_temp) {
	int arr_len = sizeof(hist_temp)/sizeof(float);
	float sum = 0.0;
	float smoothed;
	
	int i;
	for (i = 0; i <= arr_len - 2 ; i++) {
  hist_temp[i] = hist_temp[i+1]; 
		sum += hist_temp[i];
	}
	hist_temp[arr_len -1] = curr_temp;
	sum += curr_temp; 
	smoothed = sum / arr_len;
		
	return smoothed;
}

// notes in the melody:
int melody[] = {
  NOTE_C4, NOTE_G3,NOTE_G3, NOTE_A3, NOTE_G3, NOTE_B3, NOTE_C4, NOTE_C4, NOTE_G3, NOTE_C4, NOTE_A3, NOTE_C4, NOTE_G3};

// note durations: 4 = quarter note, 8 = eighth note, etc.:
int noteDurations[] = {
  4, 8, 8, 4,2,4,4,4,4,2,4,2,4,2 };


void melody_play() {
	  // iterate over the notes of the melody:
  for (int thisNote = 0; thisNote < 8; thisNote++) {

    // to calculate the note duration, take one second 
    // divided by the note type.
    //e.g. quarter note = 1000 / 4, eighth note = 1000/8, etc.
    int noteDuration = 1000/noteDurations[thisNote];
    tone(12, melody[thisNote],noteDuration);


    // to distinguish the notes, set a minimum time between them.
    // the note's duration + 30% seems to work well:
    int pauseBetweenNotes = noteDuration * 1.30;
    delay(pauseBetweenNotes);
    // stop the tone playing:
    noTone(12);
	  
  }
}
  
  
void setup() {
  Serial.begin(115200);
	pinMode(ATXGreenPIN, OUTPUT);      // sets the digital pin as output
	
	// set up the LCD's number of columns and rows: 
	lcd.begin(16, 2);
	// Print a message to the LCD.
	//lcd.setCursor (0,0);
	//lcd.print("Temperature control:");
}

void loop() {
  float temp;
  temp=smooth_temp(Thermistor(analogRead(ThermistorPIN)));       // read ADC and  convert it to Celsius
	
  Serial.print(" Celsius: "); 
	lcd.setCursor (9,1); lcd.print (" C: ");
  Serial.print(temp,1);                             // display Celsius
	lcd.setCursor (12,1); lcd.print (temp);
  //temp = (temp * 9.0)/ 5.0 + 32.0;                  // converts to  Fahrenheit
  //Serial.print(", Fahrenheit: "); 
  //Serial.print(temp,1);                             // display  Fahrenheit

if ((temp > switch_temp && prev_state == 0) || (temp > switch_temp + delta && prev_state == 1)  )
{
	digitalWrite(ATXGreenPIN, HIGH);  //switch off ATX
	prev_state = 0;
	Serial.print(" ATX OFF "); lcd.setCursor (0,0); lcd.print (" ATX OFF        ");
	
}
else
{
 	digitalWrite(ATXGreenPIN, LOW);  //switch on ATX
	prev_state = 1;
	Serial.print(" ATX ON ");  lcd.setCursor (0,0); lcd.print (" ATX ON         ");
	
	if (temp < critical_temp) {
		melody_play();  // alert for low temperature
		Serial.print(" Alert! Too low temperature"); lcd.setCursor (0,0); lcd.print ("ATX ON, LOW T!");
	}
}	
 

  Serial.println("");

//dealing with LCD
ButtonVoltage = analogRead(0);


if (ButtonVoltage > 800) ButtonPressed = BUTTON_NONE;    // No button pressed should be 1023
else if (ButtonVoltage > 500) ButtonPressed = BUTTON_SELECT;   
else if (ButtonVoltage > 400) ButtonPressed = BUTTON_LEFT;   
else if (ButtonVoltage > 250) ButtonPressed = BUTTON_DOWN;   
else if (ButtonVoltage > 100) ButtonPressed = BUTTON_UP; 
else ButtonPressed = BUTTON_RIGHT;

switch (ButtonPressed) {
case BUTTON_SELECT:
            lcd.setCursor (0,0); lcd.print ("Select      ");
            break;
case BUTTON_LEFT:
            lcd.setCursor (0,0); lcd.print ("Left        ");
            break;
case BUTTON_DOWN:     {
                lcd.setCursor (0,0); lcd.print ("Down        ");
                fadeValue = fadeValue -5;
                if (fadeValue < 5) { fadeValue = 0; }
                analogWrite (Backlight, fadeValue);
                lcd.setCursor(12,0);
                lcd.print(fadeValue);
                delay (100);
            }
            break;
case BUTTON_UP:     {
                lcd.setCursor (0,0);  lcd.print ("Up          ");
                fadeValue = fadeValue +5;
                if (fadeValue > 254) { fadeValue = 255; }
                analogWrite (Backlight, fadeValue);
                lcd.setCursor(12,0);
                lcd.print(fadeValue);
                delay (100);
            }
            break;
case BUTTON_RIGHT:
            lcd.setCursor (0,0);  lcd.print ("Right     ");
            break;
case BUTTON_NONE:
            //lcd.print ("None  ");
            break;
}

if (prev_state ==1) {
	stat_on += 1;
}
else {
	stat_off +=1;
}

lcd.setCursor (9,0);  lcd.print ("%on: ");
lcd.setCursor (13,0);  lcd.print (stat_on * 100 /(stat_on+stat_off));


  delay(1000);                                      // Delay 
}